#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    git clone https://github.com/numpy/numpy numpy
    cd numpy
    git reset --hard $version
    git log -n1
    git submodule update --init
}

build()
{
    git clone -q https://gitlab.com/Linaro/windowsonarm/packagetools

    python \
        ./packagetools/scripts/cross_compile_python_wheel.py \
        --python-version 3.10.4 \
        --source-dir .

    python \
        ./packagetools/scripts/cross_compile_python_wheel.py \
        --python-version 3.11.1 \
        --source-dir .
}

test()
{
    # to another directory to avoid conflict for "numpy" module
    mkdir run_tests
    pushd run_tests

    # https://numpy.org/doc/stable/dev/development_environment.html#running-tests
    cat > test.bat << EOF
python -m venv venv || exit 1
call venv/Scripts/activate.bat || exit 1
python -m pip install -r ../test_requirements.txt || exit 1
python -m pip install $(ls ../dist/*cp311*.whl) || exit 1

:: deactivate some tests failing
:: no support with mingw yet
rm venv/lib/site-packages/numpy/distutils/tests/test_mingw32ccompiler.py || exit 1
:: bad version, we know...
rm venv/lib/site-packages/numpy/tests/test_numpy_version.py || exit 1
python ../runtests.py --no-build -v || exit 1
EOF
    cmd /c test.bat

    popd
}

package()
{
    mv dist/*.whl $out_dir
}

checkout
build
#test
package
